Welcome to paperbackup's documentation!
====================================

:mod:`paperbackup` is a Python package for meteorological data analysis.

.. toctree::
   :maxdepth: 2
   :caption: Contents:

   install
   api/modules
   changelog


Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
