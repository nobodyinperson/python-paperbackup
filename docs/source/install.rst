Installation
============

You can install the latest development version of :mod:`paperbackup` via :mod:`pip`

.. code-block:: sh

    python3 -m pip install --user -U git+https://gitlab.com/nobodyinperson/python-paperbackup

This will install :mod:`paperbackup` directly from GitLab.

Depending on your setup it might be necessary to install the :mod:`pip` module
first:

.. code-block:: sh

    # Debian/Ubuntu
    sudo apt-get install python3-pip
    # Manjaro/Arch
    sudo pacman -Syu python-pip

Or see `Installing PIP`_.

.. _Installing PIP: https://pip.pypa.io/en/stable/installing/
