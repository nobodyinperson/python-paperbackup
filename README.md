# Python Paperbackup

[![pipeline status](https://gitlab.com/nobodyinperson/python-paperbackup/badges/master/pipeline.svg)](https://gitlab.com/nobodyinperson/python-paperbackup/-/pipelines)
[![coverage report](https://gitlab.com/nobodyinperson/python-paperbackup/badges/master/coverage.svg)](https://nobodyinperson.gitlab.io/python-paperbackup/coverage-report/)
[![documentation](https://img.shields.io/badge/documentation-here%20on%20GitLab-brightgreen.svg)](https://nobodyinperson.gitlab.io/python-paperbackup)

## Installation

To install `paperbackup` directly from GitLab, run

```bash
# make sure you have pip installed
# Debian/Ubuntu:  sudo apt update && sudo apt install python3-pip
# Manjaro/ARch:  sudo pacman -Syu python-pip
python3 -m pip install --user -U git+https://gitlab.com/nobodyinperson/python-paperbackup
```

## Documentation

Documentation can be found [here on GitLab](https://nobodyinperson.gitlab.io/python-paperbackup).
