# Contributing

Development follows the [GitLab Flow](https://about.gitlab.com/2014/09/29/gitlab-flow/). In short:

- If you want to improve something, create an issue and describe what you want to improve
- Create a branch for the issue either via the button at the issue or manually with `git switch -c ISSUENUMBER-title-of-the-issue && git push -u origin ISSUENUMBER-title-of-the-issue`
- Work on the branch as usual
- Before you commit, run `make` to be sure that everything is fine. This will build the documentation, run the tests and assert that the code is properly formatted.
- Commit and upload your changes with `git commit -a -m "Add feature bla blubb" && git push`
- If your branch is ready, create a Merge Request (GitLab will suggest a button to do that)
- In the Merge Request describe how your changes fix/improve the issue
- Somebody with merging rights will then comment and merge the request

## Code Style

Code should be formatted with [`black`](https://github.com/ambv/black) and follow [PEP8](https://www.python.org/dev/peps/pep-0008/).

Be sure to have run the following commands before pushing commits. Otherwise, the CI pipeline will only pass with warnings.

```bash
python3 -m pip install install --user black pycodestyle
# format all Python files in black style
black .
# check PEP8 compatibility
pycodestyle .
```

Or in short:

```bash
make check-codestyle
# this will check whether the code is properly formatted
```

## Build Documentation

The documentation can be built by running

```bash
# Install sphinx plugins
python3 -m pip install sphinx_rtd_theme sphinxcontrib-mermaid nbsphinx
# Create the documentation
make -C docs html
# Open the documentation
xdg-open docs/_build/html/index.html
```

Or in short:

```bash
make docs
# this will build the documentation and show it in your browser
```

## Versioning

We use [semantic versioning](https://semver.org/).

Versions are added manually as git tags.

You can see a changelog based on the annotated version tags by running this command:

```bash
git tag -l 'v*' -n99 --sort=-version:refname --format='%(color:green)%(refname:strip=2)%(color:yellow) (%(color:cyan)%(creatordate:format:%a %b %d %Y)%(color:yellow))%0a%(color:magenta)==========================%0a%0a%(color:yellow)%(contents:subject)%(color:normal)%0a%0a%(contents:body)'
```

To mark the current commit as a version, run

```bash
# start the tag with a lowercase v
# and u
git tag -a v1.2.3
```

You editor will then open. Write the version summary and **please** read [How
to Write a Git Commit Message](https://chris.beams.io/posts/git-commit/). At
least write a short first line, **then leave a blank line**, then write
whatever you think should be in the release notes.

Then publish the version tag:

```bash
git push --tags
```

## Tests

To run the tests, run

```bash
python3 -m unittest -v
```

To get a test coverage, run

```bash
# Install coverage
python3 -m pip install --user coverage
# Run the tests
python3 -m coverage run --source paperbackup -m tests -v
# Show the coverage
python3 -m coverage report
# Create a HTML report
python3 -m coverage html
# View the HTML page
xdg-open html/index.html
```

Or in short:

```bash
make coverage
# this will run the tests and open the coverage report in your browser
```

## Writing tests

For now just look at the existing tests for an example how to test something. Basically, you create a file `test_myfeature.py` in the `tests` folder which looks like this:

```python
import unittest

class MyFeatureTest(unittest.TestCase):
    """
    This class groups together tests for my feature
    """

    def test_myfeature(self):
        # here you calculate something and then you can use
        # one of the many self.assert* functions to make sure
        # that some result is equal to something else, etc...
        # Look it up in the Python unittest documentation.
        # for example:
        self.assertEqual(1,2) # this will fail obviously

    def test_my_other_feature(self):
        # you can and should write multiple tests
```
