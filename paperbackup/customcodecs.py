# system modules
import codecs
import logging
import itertools
import functools
import operator
import re
import base64
import math

# internal modules

# external modules
try:
    import base91
except ImportError:
    base91 = object()


logger = logging.getLogger(__name__)


class CustomCodec(codecs.Codec):
    """
    Base class for custom codecs

    .. note::

        This class works with the following definition:

        - You can **encode** a human-readable sequence of characters (a
          :any:`str`) to get a sequence of :any:`bytes`.
        - The way back is called **decoding**.

        This definition doesn't seem to be the standard. For example, the
        :mod:`base64` module says that :any:`base64.encodebytes` takes a
        (strangely encoded to bytes) string to return a (strangely encoded to
        bytes) bytes Base64 represenation (which should actually be a
        string...)
    """

    registry = {}
    """
    Helper dictionary to realize the search functions handed to
    :any:`codecs.register` without namespace clashes. I didn't get the search
    functions to work properly, even when creating the search functions with
    factory functions. :any:`codecs.lookup` always returned either the first or
    the last :any:`CustomCodec` subclass.
    """

    @classmethod
    def register(cls):
        """
        Add this codec to Python's codec registry
        """
        cls.registry[cls.name] = codecs.CodecInfo(
            name=cls.name, encode=cls.encode, decode=cls.decode
        )
        logger.debug("Registering [{cls.name}] codec".format(cls=cls))
        codecs.register(cls.registry.get)

    @classmethod
    def register_subclasses(cls):
        """
        Add all subclasses to Python's codec registry
        """
        for subclass in cls.__subclasses__():
            subclass.register()


class Utf8Codec(CustomCodec):
    """
    Custom UTF8 Codec
    """

    name = "utf8"

    @staticmethod
    def encode(s: str) -> bytes:
        return (s.encode("utf8"), len(s))

    @staticmethod
    def decode(b: bytes) -> str:
        return (b.decode("utf8"), len(b))


class Base64Codec(CustomCodec):
    """
    Simpler Base64 Codec without line breaks

    .. note::

        Mind the definition difference of encoding/decoding here:

        - :mod:`codec` (and in general), **encoding** means to take a
              :any:`str` and turn it into :any:`bytes`.
        - :func:`base64.encodebytes` does the opposite: it takes arbitrary
            :any:`bytes` and turns them into other :any:`bytes` that can safely
            be turned into :any:`str`!
    """

    name = "b64"

    @staticmethod
    def encode(s: str) -> bytes:
        return (base64.decodebytes(s.encode()), len(s))

    @staticmethod
    def decode(b: bytes) -> str:
        return (re.sub(r"\s+", "", base64.encodebytes(b).decode()), len(b))


class Base91Error(ValueError):
    """
    Base class for BasE91 errors
    """


# if base91 is correctly installed
if all(map(functools.partial(hasattr, base91), ("encode", "decode"))):

    class BasE91Codec(CustomCodec):
        """
        Codec for BasE91

        .. note::

            Mind the definition difference of encoding/decoding here:

            - :mod:`codec` (and in general), **encoding** means to take a
                  :any:`str` and turn it into :any:`bytes`.
            - :func:`base91.encode` does the opposite: it takes arbitrary
                :any:`bytes` and turns them into other :any:`bytes` that can
                safely be turned into :any:`str`!
        """

        name = "base91"

        @staticmethod
        def encode(s: str) -> bytes:
            invalid = set(s) - set(base91.base91_alphabet)
            if invalid:
                raise Base91Error(
                    "invalid BasE91 characters: {characters}".format(
                        characters="".join(invalid)
                    )
                )
            return (base91.decode(s), len(s))

        @staticmethod
        def decode(b: bytes) -> str:
            return (base91.encode(b), len(b))


# hook all CustomCodecs into Python's codec registry
CustomCodec.register_subclasses()
