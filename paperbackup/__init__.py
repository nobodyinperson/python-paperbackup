# system modules
import json
import io
import os
import hashlib
import math
import base64
import codecs
import itertools

# internal modules
try:
    from paperbackup.version import __version__
except ModuleNotFoundError:
    __version__ = "0.0.0"
from paperbackup import customcodecs

# external modules
import matplotlib.pyplot as plt
import numpy as np
import qrcode
import attr


@attr.s
class Nr:
    """
    Representation of a count with an optional total. Count is one-based,
    thus zero means undefined (the default).
    """

    count = attr.ib(default=0, converter=int)
    """
    The count. Can't be greater than total.
    """

    @count.validator
    def validate(instance, attribute, value):
        if value < 0:
            raise ValueError("nr must be positive")
        if instance.total:
            if value > instance.total:
                raise ValueError("nr can't be greater than total")

    total = attr.ib(default=0, converter=int)
    """
    The total. Can't be smaller than the nr.
    """

    @classmethod
    def from_(cls, obj):
        if isinstance(obj, cls):
            return obj
        if isinstance(obj, dict):
            return cls(**obj)
        try:
            args = tuple(iter(obj))
            return cls(*args)
        except TypeError:
            try:
                return cls(count=obj)
            except (TypeError, ValueError):
                pass
        raise ValueError(
            f"Don't know how to turn {repr(obj)} to {cls.__name__}"
        )

    def __bool__(self):
        return bool(self.count)


@attr.s()
class Chunk:
    """
    Representation of a numbered chunk of a bigger payload
    """

    nr = attr.ib(converter=Nr.from_, factory=Nr)


@attr.s
class BytesChunk(Chunk):
    payload = attr.ib(
        converter=bytes,
        factory=bytes,
        validator=attr.validators.instance_of(bytes),
    )

    def decode(self, encoding):
        return StringChunk(
            nr=self.nr,
            encoding=encoding,
            payload=codecs.decode(self.payload, encoding),
        )

    def decode_smallest(self):
        def usable_encodings():
            for encoding in customcodecs.CustomCodec.registry:
                try:
                    yield self.decode(encoding)
                except (UnicodeDecodeError, ValueError, TypeError):
                    continue

        return min(
            usable_encodings(),
            key=lambda c: len(c.payload),
        )


@attr.s
class StringChunk(Chunk):
    encoding = attr.ib(default=None)
    payload = attr.ib(factory=str, validator=attr.validators.instance_of(str))

    def encode(self):
        return BytesChunk(
            nr=self.nr,
            payload=codecs.encode(self.payload, self.encoding),
        )

    def to_json(self):
        def serialize(inst, field, value):
            if isinstance(value, Nr):
                return attr.astuple(value) if value.total else value.count
            return value

        return attr.asdict(self, value_serializer=serialize)


def read_chunked(fh, chunksize):
    count = itertools.count(1)
    bytes_read = 0
    while not fh.closed:
        payload = fh.read(chunksize)
        bytes_read += len(payload)
        n_chunk = next(count)
        try:
            total_bytes = os.path.getsize(fh.name)
            total_chunks = math.ceil(max(bytes_read, total_bytes) / chunksize)
        except (io.UnsupportedOperation, OSError, TypeError):
            total_chunks = 0
        if not payload:
            return
        chunk = BytesChunk(
            nr=Nr(count=n_chunk, total=total_chunks), payload=payload
        )
        yield chunk


def qrcode_figure(chunks, nrows, ncols):
    md5 = hashlib.md5()
    while True:
        fig, axes = plt.subplots(figsize=(8.3, 11.7), ncols=ncols, nrows=nrows)
        for ax in np.array(axes).flatten():
            ax.axis("off")
        for ax in np.array(axes).flatten():
            try:
                chunk = next(chunks)
                md5.update(chunk.payload)
            except StopIteration:
                fig.text(
                    x=0.5,
                    y=0.01,
                    horizontalalignment="center",
                    s="MD5 sum of original file: {}".format(md5.hexdigest()),
                    fontdict={"family": "monospace"},
                )
                yield fig
                return
            chunk_string = chunk.decode_smallest()
            qr = qrcode.QRCode(border=1)
            chunk_json = json.dumps(chunk_string.to_json())
            qr.add_data(chunk_json)
            qrmatrix = np.array(qr.get_matrix())
            ax.imshow(qrmatrix, cmap="Greys", interpolation="none")
            ax.set_title(f"{chunk.nr.count} / {chunk.nr.total or '-'}")
        yield fig
