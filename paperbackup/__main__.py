# system modules
import sys
import hashlib
import json
import logging
import datetime
import collections
import os

# internal modules
import paperbackup

# external modules
import click
import matplotlib
import matplotlib.pyplot as plt
from matplotlib.backends.backend_pdf import PdfPages
from tqdm.auto import tqdm


logger = logging.getLogger(__name__)


@click.group(
    help="Turn files to PDFs with QR-codes and vice-versa",
    context_settings={"help_option_names": ["-h", "--help"]},
)
@click.option("-v", "--verbose", is_flag=True)
@click.pass_context
def cli(ctx, verbose):
    logging.basicConfig(level=logging.DEBUG if verbose else logging.WARNING)


@cli.command(help="Turn file to PDF")
@click.option(
    "-i",
    "--input",
    "input_fh",
    type=click.File("rb"),
    default="-",
    help="Input file",
)
@click.option(
    "-o",
    "--output",
    "output_fh",
    type=click.File("wb"),
    default="-",
    help="Output file",
)
@click.option(
    "--chunksize",
    type=click.IntRange(min=1),
    default=120,
)
@click.option(
    "-r",
    "--rows",
    "nrows",
    type=click.IntRange(min=1),
    default=5,
)
@click.option(
    "-c",
    "--cols",
    "ncols",
    type=click.IntRange(min=1),
    default=5,
)
@click.option(
    "--json",
    "output_json_chunks",
    is_flag=True,
    help="Output the JSON chunks",
)
@click.option(
    "--progress/--no-progress",
    is_flag=True,
    default=True,
    help="Display progress bar",
)
@click.pass_context
def encode(
    ctx,
    input_fh,
    output_fh,
    chunksize,
    nrows,
    ncols,
    output_json_chunks,
    progress,
):
    with tqdm(unit="chunks", disable=not progress) as pbar:

        def chunks():
            for chunk in paperbackup.read_chunked(
                input_fh, chunksize=chunksize
            ):
                pbar.total = chunk.nr.total or (chunk.nr.count + 1)
                pbar.update()
                yield chunk

        if output_json_chunks:
            for chunk in chunks():
                click.echo(json.dumps(chunk.decode_smallest().to_json()))
            ctx.exit()

        with matplotlib.style.context({"font.size": 8}):
            with PdfPages(output_fh) as pdf:
                for page, fig in enumerate(
                    paperbackup.qrcode_figure(
                        chunks(), nrows=nrows, ncols=ncols
                    ),
                    start=1,
                ):
                    pbar.set_description(f"PDF page {page}")
                    if page == 1:
                        fig.suptitle(
                            "{} - {}".format(
                                os.path.basename(
                                    getattr(input_fh, "name", "?")
                                ),
                                datetime.datetime.utcnow()
                                .replace(tzinfo=datetime.timezone.utc)
                                .strftime("%Y-%m-%d %H:%M:%S%z"),
                            ),
                            fontdict={"family": "monospace"},
                        )
                    width, height = fig.get_size_inches()
                    wider, smaller = 1 / width, 0.25 / width
                    fig.tight_layout()
                    fig.subplots_adjust(
                        left=wider if page % 2 else smaller,
                        right=(1 - smaller) if page % 2 else (1 - wider),
                        # top=1 - (0.25 / height),
                        # bottom=0.25 / height,
                        bottom=0.05,
                    )
                    pdf.savefig(fig)
                    plt.close(fig)


@cli.command()
@click.option(
    "-i",
    "--input",
    "input_fh",
    type=click.File("r"),
    default="-",
    help="Input file",
)
@click.option(
    "-o",
    "--output",
    "output_fh",
    type=click.File("wb"),
    default="-",
    help="Output file",
)
@click.option("--from-chunks", is_flag=True)
@click.pass_context
def decode(ctx, input_fh, output_fh, from_chunks):
    """
    Restore file from chunks

    Automatic Example:

    zbarimg --raw -Sdisable --quiet -Sqrcode.enable scan*.png
        | paperbackup decode --from-chunks -o output_file

    Manual Example:

    zbarcam --raw | paperbackup decode --from-chunks

    Then scan all QR codes in an arbitrary order.
    """
    if not from_chunks:
        ctx.fail("Only decoding --from-chunks is currently implemented")
    chunks_on_hold = list()
    chunks_total = 0
    counter = collections.defaultdict(int)

    md5 = hashlib.md5()

    with tqdm(unit="chunks", position=0) as read_pbar, tqdm(
        unit="chunks", position=1
    ) as write_pbar:
        write_pbar.set_description("chunks written")

        def output_chunk(chunk):
            if not isinstance(chunk.payload, bytes):
                chunk = chunk.encode()
            logger.debug(f"Writing {chunk}")
            md5.update(chunk.payload)
            output_fh.write(chunk.payload)
            write_pbar.update()
            counter["chunks-written"] += 1

        while True:
            if chunks_total:
                if counter["chunks-written"] == chunks_total:
                    logger.debug(f"All {chunks_total} recieved")
                    ctx.exit()
            try:
                line = next(input_fh)
            except StopIteration:
                ctx.fail(
                    "Input ended before {} chunks were recieved".format(
                        f"all {chunks_total}"
                        if chunks_total
                        else ("all" if counter["chunks-written"] else "any")
                    )
                )
            try:
                chunk_json = json.loads(line)
            except BaseException as e:
                logger.error(f"Skipping invalid JSON line {repr(line)}: {e}")
                continue
            try:
                string_chunk = paperbackup.StringChunk(**chunk_json)
            except BaseException as e:
                logger.error(
                    f"Skipping invalid chunk JSON {repr(chunk_json)}: {e}"
                )
                continue
            if string_chunk.nr.total:
                if string_chunk.nr.total > chunks_total:
                    chunks_total = string_chunk.nr.total
                    read_pbar.total = chunks_total
                    write_pbar.total = chunks_total
            if string_chunk.nr.count < counter["chunks-written"]:
                logger.debug(
                    f"We have written {counter['chunks-written']} chunks "
                    f"but now we recieved chunk {string_chunk.nr}. "
                    "Ignoring it."
                )
                continue
            try:
                bytes_chunk = string_chunk.encode()
            except BaseException as e:
                ctx.fail(f"Couldn't decode {string_chunk}: {e}")
            if not bytes_chunk.nr:
                read_pbar.update()
                output_chunk(bytes_chunk)
                continue
            if bytes_chunk not in chunks_on_hold:
                chunks_on_hold.append(bytes_chunk)
                read_pbar.set_description(
                    f"{len(chunks_on_hold)} chunks on hold"
                )
                read_pbar.update()
            else:
                logger.debug(f"Chunk {bytes_chunk} already on hold")
            chunks_on_hold.sort(key=lambda c: c.nr.count)
            for chunk_on_hold in chunks_on_hold.copy():
                if chunk_on_hold.nr.count == counter["chunks-written"] + 1:
                    output_chunk(chunk_on_hold)
                    chunks_on_hold.pop(chunks_on_hold.index(chunk_on_hold))
                    read_pbar.set_description(
                        f"{len(chunks_on_hold)} chunks on hold"
                    )
                    continue

    logger.info(f"MD5sum: {md5.hexdigest()}")


if __name__ == "__main__":
    cli(prog_name="{} -m paperbackup".format(os.path.basename(sys.executable)))
